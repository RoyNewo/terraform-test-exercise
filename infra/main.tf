terraform {
  backend "s3" {
    bucket  = "s3-terraform-state-bucket"
    region  = "eu-west-1"
    key     = "terraform-test-exercise.tfstate"
    encrypt = true # No se obligatorio y esta usando la clave por defecto pero no cuesta nada ponerlo

  }
}
provider "aws" {
  region  = "eu-west-1"
  profile = var.aws_profile
  assume_role {
    role_arn = "arn:aws:iam::blabla:role/adminrole"
  }
}

module "ipfs1" {
    source = "../module"
    name   = "ipfs1"
    ami    = var.ami
    instance_type = var.instance_type
    key_name = var.key_name
    availability_zone = "eu-west-1a"
    subnet_id = "private-subnetid-1"
    vpc_security_group_ids = [aws_security_group.sp-ipfs-arn.id]
    device_name = "/dev/sdb"
    size = 100
}
module "ipfs2" {
    source = "../module"
    name   = "ipfs2"
    ami    = var.ami
    instance_type = var.instance_type
    key_name = var.key_name
    availability_zone = "eu-west-1b"
    subnet_id = "private-subnetid-2"
    vpc_security_group_ids = [aws_security_group.sp-ipfs-arn.id]
    device_name = "/dev/sdb"
    size = 100
}
module "ipfs3" {
    source = "../module"
    name   = "ipfs3"
    ami    = var.ami
    instance_type = var.instance_type
    key_name = var.key_name
    availability_zone = "eu-west-1c"
    subnet_id = "private-subnetid-3"
    vpc_security_group_ids = [aws_security_group.sp-ipfs-arn.id]
    device_name = "/dev/sdb"
    size = 100
}