resource "aws_volume_attachment" "ebs_ipfs1" {
  device_name = var.device_name
  volume_id   = aws_ebs_volume.ipfs1.id
  instance_id = aws_instance.ipfs1.id
}

resource "aws_instance" "ipfs1" {

  ami                         = var.ami
  associate_public_ip_address = var.associate_public_ip_address
  instance_type               = var.instance_type
  key_name                    = var.key_name
  availability_zone           = var.availability_zone
  vpc_security_group_ids      = [aws_security_group.sp-ipfs-arn.id]
  subnet_id                   = var.subnet_id
}

resource "aws_ebs_volume" "ipfs1" {
  availability_zone = var.availability_zone
  size              = var.size
}

