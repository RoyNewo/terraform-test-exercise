# Variables
variable "aws_profile" {
  description = "The AWS profile to use"
  type        = string
  default     = "default"
}
variable "device_name" {
  description = "The device name to use in the attachement"
  type        = string
  default     = "/dev/sdb"
}

variable "ami" {
  description = "The AMI to use for the server"
  type        = string
  default     = "ami-abcd1234"
}

variable "instance_type" {
  description = "The instance type to use for the server"
  type        = string
  default     = "t2.medium"
}

variable "key_name" {
  description = "The key name to use for the server"
  type        = string
  default     = "ipfs-keypair-name"
}

variable "associate_public_ip_address"{
    description = "Associate a public IP address with the instance"
    type        = bool
    default     = false
}

variable "subnet_id" {
  description = "The subnet ID to use for the server"
  type        = string
}

variable "availability_zone" {
  description = "The availability zone to use for the server"
  type        = string
}

variable "size" {
  description = "The size of the EBS volume"
  type        = number
  default     = 100
}