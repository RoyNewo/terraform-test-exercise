provider "aws" {
  region  = "eu-west-1"
  profile = var.aws_profile
  assume_role {
    role_arn = "arn:aws:iam::blabla:role/adminrole"
  }
}


backend "s3" {
  bucket  = "s3-terraform-state-bucket"
  region  = "eu-west-1"
  key     = "security-group.tfstate"
  encrypt = true # No se obligatorio y esta usando la clave por defecto pero no cuesta nada ponerlo

}
