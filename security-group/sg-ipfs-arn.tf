resource "aws_security_group" "sp-ipfs-arn" {
  name        = "sg-ipfs-arn"
  description = "Security group for ec2"
  ingress {
    from_port   = 4001
    to_port     = 4001
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Default"
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Default"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Default"
  }
}

#
# OUTPUT
# 
output "sg-sp-ipfs-arn" {
  value = aws_security_group.basico.id
}

# Lo que yo haria para reconcialar el estado de terraform con el estado real de AWS es lo siguiente:
# 1. Crearia un fichero de terraform con el nombre de sg-ipfs-arn.tf y el contenido seria solo:
#     resource "aws_security_group" "sp-ipfs-arn" {}
# 2. Haria un import del security group ya que exite en aws:
#     terraform import aws_security_group.sp-ipfs-arn sp-ipfs-arn

# 3. Haria un terraform show para ver el estado y de este evitaria cosas como el arn o el owner id que no se pueden poner en el fichero de terraform
# 4. añadiria la informacion al archivo de terraform y le daria un poco de formato
# 5. Comprobaria con terraform plan que todo este correcto
# 6. Aplicaria los cambios con terraform apply si todo esta correcto no habra realmente nada que aplicar